Password Store integration for awesomewm
---

I use [pass](https://www.passwordstore.org/) as my general password manager. It's great.

I use [awesomewm](https://awesomewm.org/) as my window manager. It's great.

I've always wanted something like `passmenu` that comes bundled with `pass`, but using
awesomewm native widgets instead of dmenu, so it integrates with my theme better.

Based roughly on the existing `menubar`, I've created an awesomewm-native bar for
interacting with `pass`.

## Installation

```bash
cd $HOME/.config/awesome
git clone https://gitlab.com/rperce/passbar.git
```

Then, wherever you manage globalkeys, add a binding to run `passbar`:
```lua
local passbar = require("passbar")

globalkeys = awful.util.table.join(
    -- [snip]
    awful.key({ modkey }, "p", function() passbar.show() end,
              {description = "show the passbar", group = "launcher"}),
    -- [snip]
)
```

I don't use `menubar`, so I replaced Mod4+p with this. You may want a different
keybinding if you do use `menubar`.
